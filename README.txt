BUSSON Louis TP2 - Compte rendu TP de programmation système

Implémentation des fonctions de la glibc grâce à l'utilisation des appels systèmes.

Question 5 :
Initialiser le buffer avec '\0' permet de ne pas avoir à rajouter un '\0' à la fin a chaque fois
que le buffer est utilisé. On peut juste ecrire dans le buffer sans se soucier car il y aura toujours
un '\0' à la fin sauf si le buffer est completement rempli, seul cas à prendre en compte lors de 
son utilisation 

Question 6 :
La fonction free reduit la taille mémoire du programme en désallouant de la taille de la zone mémoire
passée en paramètre.Ce qui a été stockée dans cet espace mémoire l'est toujours, il n'est juste plus
utilisable par le programme

Question 17 :
Quand la chaine de caractère ne contient pas de retour à la ligne et qu'elle ne rempli pas
completement le buffer alors elle n'est jamais affichée et reste stockée dans le buffer.

Question 18 :
On modifie mini_exit pour vider tous les buffer pour cela on peut utiliser une autre fonction, 
dans mon cas mini_exit_string qui permet de vider le buffer de printf.

Question 20 :
Si le nombre de caractère saisi est égale à la taille du buffer on remplace le caractère '\0', pour
règler ce problème il faut lire size_buffer moins 1 caractère pour garder un '\0' à la fin

Question 22 :
Il faut éviter de copier une chaine source plus grande que la chaine de destination sinon on risque
le buffer overflow et d'écrire dans un espace mémoire non alloué.
Il faut faire attention à la taille des chaine manipulée.

Question 34:
Si le buffer n'est pas plein et que le programme se termine alors ce qui est stocké dans le buffer
ne sera jamais écris dans le fichier pour cela on fait appel à mini_fflush dans mini_exit_io qui est
lui même appelé dans mini_exit

