#include "mini_lib.h"

#include <stdio.h>

#include <unistd.h> 

struct malloc_element{
   void* buffer;
   int size;
   int state;
   struct malloc_element* next;
};

static struct malloc_element* malloc_list = NULL;

void* mini_calloc(int size_element, int number_element)
{
   if(size_element < 0 && number_element < 0){
      mini_printf("Calloc bad arguments !");
      return NULL;
   }
   struct malloc_element* index = malloc_list;

   while(index !=  NULL)
   {
      if(index->size >= size_element*number_element && index->state == 0)
      {
         index->state=1;
         char* tmp = index->buffer;
         for(int i=0; i<index->size; i++){
            *(tmp+i) = '\0';
         }
         return index->buffer;
      }
      index = index->next;  
   }
   struct malloc_element* element;
   element = sbrk(sizeof(struct malloc_element));
   element->buffer = sbrk(size_element*number_element);
   element->size = size_element*number_element;
   element->state = 1;
   char* tmp = element->buffer;
      for(int i=0; i<element->size; i++){
         *(tmp+i) = '\0';
      }
   element->next=malloc_list;
   malloc_list=element;
   
   return element->buffer;
}


void mini_free(void* ptr)
{
   if(ptr == NULL) mini_printf("Pointer of free is NULL");
   struct malloc_element* i = malloc_list;
   while (i->buffer != ptr && i != NULL)
   {
      i = i->next;
   }
   if(i->buffer == ptr && i->state != 0) i->state = 0;
}

void mini_exit()
{
   mini_exit_string();
   mini_exit_io();
   _exit(0);
}