#include "mini_lib.h"

int main(int argc, char *argv[])
{
   if(argc > 1){
      if(mini_strcmp(argv[1], "touch") == 1) mini_touch(argv[2]);
      else if(mini_strcmp(argv[1], "cp") == 1) mini_cp(argv[2], argv[3]);
      else if(mini_strcmp(argv[1], "cat") == 1) mini_cat(argv[2]);
      else if(mini_strcmp(argv[1], "head") == 1) mini_head(argv[2], atoi(argv[3]));
      else if(mini_strcmp(argv[1], "tail") == 1) mini_tail(argv[2], atoi(argv[3]));
      else if(mini_strcmp(argv[1], "clean") == 1) mini_clean(argv[2]);
      else if(mini_strcmp(argv[1], "grep") == 1) mini_grep(argv[2], argv[3]);
      else if(mini_strcmp(argv[1], "wc") == 1) mini_wc(argv[2]);
   }
   else {
      char* chaine;
      chaine = mini_calloc(1, 100);
      mini_printf("Ceci est un scénario pour tester quelques commandes implementées.\n");
      mini_printf("Les autres commandes peuvent être appelées en les passant en argument en éxecutant le programme\n");
      mini_printf("Test du mini scanf, entrez un nom de fichier à créer:\n");
      mini_scanf(chaine, 100);
      mini_printf("Le nom de fichier entré est :\n");
      mini_printf(chaine);
      
      MYFILE *file;
      chaine[mini_strlen(chaine)] = '\0'; //To avoid \n introduce by scanf in file name
      mini_touch(chaine);
      file = mini_fopen(chaine, 'b');

      char* buffer1;
      buffer1 = mini_calloc(1, 100);
      mini_printf("Entrez le contenu du fichier ");
      mini_printf(chaine);
      mini_scanf(buffer1, 100);
      mini_fwrite(buffer1, 1, 100, file);
      mini_fclose(file);

      file = mini_fopen(chaine, 'b');
      char* buffer2;
      buffer2 = mini_calloc(1, 100);
      mini_printf("Lecture du fichier :\n");
      mini_fread(buffer2, 1, 100, file);
      mini_printf(buffer2);
      mini_printf("\n");
      mini_fclose(file);
      
      mini_printf("Exemple de fgetc et fputc : copie du premier caractère du fichier créé dans exemple.txt\n");
      MYFILE *src, *dest;
      src = mini_fopen(chaine, 'r');
      dest = mini_fopen("exemple.txt", 'w');
      char *c;
      c = mini_calloc(1, 2);
      *c = mini_fgetc(src);
      mini_printf(c);
      mini_printf("\n");
      mini_fputc(dest, *c);
      mini_fclose(src);
      mini_fclose(dest);
   
      mini_echo("Ceci est un test d'echo \n en plusieurs lignes");
   }
   mini_exit();
   return 0;
}
