#include "mini_lib.h"
#include <fcntl.h>
#include <unistd.h>

#define FILE_LIST_SIZE 20

static MYFILE* file_list[FILE_LIST_SIZE];
static int ind_file_list = 0;

MYFILE* mini_fopen(char* file, char mode)
{
   MYFILE *f;
   f = mini_calloc(sizeof(MYFILE), 1);
   int m;

   switch (mode)
   {
   case 'r':
      m = O_RDONLY;
      break;
   case 'w':
      m = O_WRONLY;
      break;
   case 'b':
      m =O_RDWR;
      break;
   case 'a':
      m=O_APPEND;
      break;

   case 't':
      m=O_TRUNC;
      break;
   default:
      mini_perror("Aucun mode valable");
      break;
   }
   f->fd = open(file, m | O_CREAT, 0755);
   if(f->fd == -1) mini_perror("Erreur d'ouverture");
   else if(ind_file_list == FILE_LIST_SIZE) mini_printf("Too many files already open\n");
   else{
      file_list[ind_file_list] = f;
      ind_file_list++;
      f->ind_read = -1;
      f->ind_write = -1;
   }
   return f;
 }

 int mini_fread(void *buffer, int size_element, int number_element, MYFILE* file)
 {
   if(size_element < 0 || number_element < 0 || file == NULL){
      mini_perror("fread : bad arguments !");
      return -1;
   }
   if(file->ind_read == -1){
      file->buffer_read = mini_calloc(1, IOBUFFER_SIZE);
      if(file->buffer_read == NULL) mini_perror("fread : buffer error ");
      file->ind_read = 0;
      if(read(file->fd, file->buffer_read, IOBUFFER_SIZE) == -1){
         mini_perror("fread : read error");
         return -1;
      }
   }
   for(int i = 0; i < (size_element*number_element)-1; i++){
      if(file->ind_read == IOBUFFER_SIZE){
         file->ind_read = 0;
         if(read(file->fd, file->buffer_read, IOBUFFER_SIZE) == -1){
            mini_perror("fread : re-read error");
            return -1;
         }      
      }
      char *tmpbuf=buffer;
      char *tmpbufread=file->buffer_read;
      if(tmpbufread[i] == 0) break;
      tmpbuf[i] = tmpbufread[file->ind_read];
      file->ind_read++;
   }
   return file->ind_read;
 }
 
int mini_fwrite(void *buffer, int size_element, int number_element, MYFILE* file)
{
   if(size_element < 0 || number_element < 0 || file == NULL){
      mini_perror("fwrite : bad arguments !");
      return -1;
   }
   if(file->ind_write == -1){
      file->buffer_write = mini_calloc(1, IOBUFFER_SIZE);
      if(file->buffer_write == NULL) mini_perror("fwrite : buffer error ");
      file->ind_write = 0;
   }
   for (int i = 0; i < mini_strlen(buffer); i++)
   {
      if(file->ind_write == IOBUFFER_SIZE){
         if(write(file->fd, file->buffer_write, IOBUFFER_SIZE) == -1){
            mini_perror("fwrite : write error !");
            return -1;
         }
         file->ind_write = 0;
      }
      char *tmpbuf=buffer;
      char *tmpbufwrite=file->buffer_write;
      tmpbufwrite[file->ind_write] = tmpbuf[i];
      file->ind_write++;
   }
   return size_element*number_element;
}

int mini_fflush(MYFILE * file)
{
   if(file == NULL){
      mini_perror("fflush : bad arguments");
      return -1;
   }
   if(file->ind_write != -1){
      int i;
      i = write(file->fd, file->buffer_write, file->ind_write);
      if(i < 0){
         mini_perror("fflush : write error");
         return -1;
      }
      else return i;
   }
   return 0; //Pour les fichiers ouverts seulement en lecture qui n'ont pas besoin d'être flush (i.e. buffer_read n'est pas instancié)
}

int mini_fclose(MYFILE* file)
{
   if(file == NULL){
      mini_perror("fclose : bad argument");
      return -1;
   }
   mini_fflush(file);
   int i;
   for (i = 0; file_list[i] != file; i++);
   for (int j = i; j < ind_file_list-1; j++)
   {     
      file_list[j] = file_list[j+1];
   }
   ind_file_list += -1;
   return close(file->fd);
   if(file->ind_read != -1) mini_free(file->buffer_read);
   if(file->ind_write != -1) mini_free(file->buffer_write);
   mini_free(file);
}

int mini_fgetc(MYFILE* file)
{
   char *c;
   c = mini_calloc( 1, 2); //2 for \0
   int result = mini_fread(c, 1, 2, file);
   if(result < 0) return -1;
   else return c[0];
}

int mini_fputc(MYFILE* file, char c)
{
   return mini_fwrite(&c, 1, 1, file);
}


void mini_exit_io()
{
   for (int i = 0; i < ind_file_list; i++)
   {
      mini_fflush(file_list[i]);
      if(file_list[i]->ind_read != -1) mini_free(file_list[i]->buffer_read);
      if(file_list[i]->ind_write != -1) mini_free(file_list[i]->buffer_write);
      mini_free(file_list[i]);
   }
   
}
