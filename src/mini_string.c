#include "mini_lib.h"

#include <unistd.h>
#include <errno.h>

//Pour le numéro d'erreur
#include <stdio.h>

#define IND -1
#define BUF_SIZE 1024

static char* buffer;
static int ind = IND;

void mini_printf(char* chaine)
{
	if(ind == -1){
		buffer = mini_calloc(BUF_SIZE, 1);
		if(buffer == NULL){
			//mini_perror("Erreur");
		}
		ind = 0;
	}
	for(int i=0; chaine[i] != '\0'; i++){
		buffer[ind] = chaine[i];
		ind++;
      if(chaine[i] == '\n' || ind == BUF_SIZE){
        	if(write(STDOUT_FILENO, buffer, ind) == -1)
			   mini_perror("Erreur printf :");
			ind = 0;
         for (int i = 0; i < BUF_SIZE; i++)
         {
            buffer[i] = '\0';
         }
         
		}
	}
}

int mini_scanf(char* buffer, int size_buffer)
{
   int i = 0;
   while(i < size_buffer-1 && read(STDIN_FILENO, buffer+i, 1) > 0)
   {
      if(buffer[i] == '\n')
         break;
      i++;
   }
   return i;
}

int mini_strlen(char* s)
{
   int i;
   for (i = 0; s[i] != '\0'; i++);
   return i;
}

int mini_strcpy(char* s, char*d)
{
   int i = 0;
   while (s[i] != '\0')
   {
      d[i] = s[i];
      i++;
   }
   //d[i] = '\0';
   return i;
}

int mini_strcmp(char* s1, char* s2)
{
   if(mini_strlen(s1) != mini_strlen(s2)) return -1;
   int i = 0;
   while (s1[i] != '\0' && s2[i] != '\0')  
   {
      if(s1[i] != s2[i]) return -1;
      i++;
   }
   return 1;
}

void mini_perror(char* message)
{
   mini_printf(message);
   mini_printf("\n");
   printf("%i\n", errno);
}

char* mini_itoa(int integer)
{
   if(integer < 0){
      perror("mini_itoa : not working for negative integer");
      return 0;
   } else {
      char* string;
      int nb_digit=0, tmp, i=1, j=0;
      tmp = integer;
      while (tmp >= 1)
      {
         nb_digit++;
         tmp = tmp/10;
         i=i*10;
      }
      string = mini_calloc(1, nb_digit+1);
      i=i/10;
      while(j < nb_digit)
      {
         string[j] = 48 + (integer/i);
         integer -= (integer/i)*i;
         i=i/10;
         j++;
      }
      return string;
   }
}

void mini_exit_string()
{
   if(buffer){
      write(STDOUT_FILENO, buffer, ind);
      mini_printf("\n");
   }
}