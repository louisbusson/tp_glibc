#define IOBUFFER_SIZE 2048

typedef struct
{
   int fd;
   void* buffer_read;
   void* buffer_write;
   int ind_read;
   int ind_write;
} MYFILE;

void* mini_calloc(int size_element, int number_element);
void mini_free(void* ptr);
void mini_exit();

void mini_printf(char* chaine);
int mini_scanf(char* buffer, int size_buffer);
int mini_strlen(char* s);
int mini_strcpy(char* s, char*d);
int mini_strcmp(char* s1, char* s2);
void mini_perror(char* message);
char* mini_itoa(int integer);
void mini_exit_string();

MYFILE* mini_fopen(char* file, char mode);
int mini_fread(void* buffer, int size_element, int number_element, MYFILE* file);
int mini_fwrite(void* buffer, int size_element, int number_element, MYFILE* file);
int mini_fflush(MYFILE * file);
int mini_fclose(MYFILE* file);
int mini_fgetc(MYFILE* file);
int mini_fputc(MYFILE* file, char c);
void mini_exit_io();

int mini_touch(char* file_name);
void mini_echo(char* chaine);
void mini_cp(char* src, char* dest);
void mini_cat(char* file_name);
void mini_head(char* file_name, int N);
void mini_tail(char* file_name, int N);
void mini_clean(char* file_name);
void mini_grep(char* file_name, char* word);
void mini_wc(char* file_name);
