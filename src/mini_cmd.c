#include "mini_lib.h"

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <fcntl.h>

int mini_touch(char *file_name)
{
   if (file_name == NULL)
   {
      mini_perror("touch : bad arguments !");
      return -1;
   }
   else
   {
      MYFILE *file;
      file = mini_fopen(file_name, 'b');
      return mini_fclose(file);
   }
}

void mini_cp(char *src, char *dest)
{
   if (src == NULL || dest == NULL)
      mini_perror("cp : bad arguments !");
   else
   {
      MYFILE *file_src, *file_dest;
      file_src = mini_fopen(src, 'r');
      file_dest = mini_fopen(dest, 'w');
      struct stat st;
      if (stat(src, &st) != 0)
      {
         mini_perror("cp : file size error");
      }
      else
      {
         for (int i = 0; i < st.st_size; i++)
         {
            mini_fputc(file_dest, mini_fgetc(file_src));
         }
      }
      mini_fclose(file_src);
      mini_fclose(file_dest);
   }
}

void mini_echo(char *chaine)
{
   mini_printf(chaine);
}

void mini_cat(char *file_name)
{
   if (file_name == NULL)
      mini_perror("cat : bad arguments !");
   else
   {
      MYFILE *file;
      file = mini_fopen(file_name, 'r');
      char *c;
      c = mini_calloc(1, 2);
      c[0] = 1;
      while (c[0] != 0)
      {
         c[0] = mini_fgetc(file);
         mini_printf(c);
      }
   }
}

void mini_head(char *file_name, int N)
{
   if (file_name == NULL || N <= 0)
      mini_perror("head : bad arguments !");
   else
   {
      MYFILE *file;
      file = mini_fopen(file_name, 'r');
      char *c;
      int i = 0;
      c = mini_calloc(1, 2);
      c[0] = 1;
      while (c[0] != 0 && i < N)
      {
         c[0] = mini_fgetc(file);
         if (c[0] == '\n')
            i++;
         mini_printf(c);
      }
      mini_fclose(file);
   }
}

void mini_tail(char *file_name, int N)
{
   if (file_name == NULL || N <= 0)
      mini_perror("tail : bad arguments !");
   else
   {
      MYFILE *file;
      file = mini_fopen(file_name, 'r');
      char *c;
      int total_line = 0, offset = 0;
      c = mini_calloc(1, 2);
      c[0] = 1;
      while (c[0] != 0)
      {
         c[0] = mini_fgetc(file);
         if (c[0] == '\n')
            total_line++;
      }
      if (N < total_line)
         offset = total_line - N;
      lseek(file->fd, 0, SEEK_SET);   // Appel système pour repostionner le curseur au début du fichier
      file->ind_read = IOBUFFER_SIZE; // Permet de forcer un appel à read pour mini_fread
      c[0] = 1;
      while (c[0] != 0)
      {
         c[0] = mini_fgetc(file);
         if (offset == 0)
            mini_printf(c);
         else if (c[0] == '\n')
            offset--;
      }
      mini_fclose(file);
   }
}

void mini_clean(char *file_name)
{
   if (file_name == NULL)
      mini_perror("clean : bad arguments !");
   else
   {
      MYFILE *file;
      file = mini_fopen(file_name, 't');
      if(file == NULL) mini_perror("clean : open error !");
      mini_fclose(file);
   }
}

void mini_grep(char *file_name, char *word)
{
   if (file_name == NULL || word == NULL)
      mini_perror("grep : bad arguments !");
   else
   {
      MYFILE *file;
      char c, *l;
      int word_size, found = 0, match_index = 0, line_size = 0;

      file = mini_fopen(file_name, 'r');
      word_size = mini_strlen(word);

      c = mini_fgetc(file);
      l = file->buffer_read + file->ind_read;

      while (c != 0)
      {
         if (c == word[match_index])
            match_index++;
         else
            match_index = 0;

         if (match_index == word_size)
         {
            found = 1;
            match_index = 0;
         }

         if (c == '\n')
         {
            if (found)
            {
               l[line_size] = '\0';
               mini_printf(l);
               mini_printf("\n");
               found = 0;
            }
            l = file->buffer_read + file->ind_read;
            line_size = -1;
         }

         c = mini_fgetc(file);
         line_size++;
      }

      mini_fclose(file);
   }
}

void mini_wc(char* file_name)
{
   if (file_name == NULL)
      mini_perror("wc : bad arguments !");
   else
   {
      MYFILE *file;
      char prevChar=0, currChar;
      int count = 0;

      file = mini_fopen(file_name, 'r');
      currChar = mini_fgetc(file);

      while (currChar != 0)
      {
         prevChar = currChar;
         currChar = mini_fgetc(file);
         if ((currChar == ' ' && prevChar != ' ' && prevChar != '\n') || 
               (currChar == '\n' && prevChar != ' ' && prevChar !='\n'))
            count++;
      }
      if (prevChar != 0 && prevChar != ' ' && prevChar != '\n')
         count++;
      
      mini_printf(mini_itoa(count));
      mini_printf("\n");
      mini_fclose(file);
   }
}


